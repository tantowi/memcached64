; *******************************************************************
; MEMCACHED FOR WINDOWS 64 bit
;
; Binary is from 
;   http://downloads.northscale.com/memcached-1.4.5-amd64.zip
;
; Copyright (C) 2015 M Tantowi Mustofa
; ttw@tantowi.com
;
; *******************************************************************


; Version Information
#define MyAppName "Memcached64"
#define MyAppVer "1.4.4"
#define MyAppDir "Memcached64"

#define Welcome1 "Setup will install [name/ver] on this computer.%n%n%n%n%n"
#define Welcome2 "Copyrightę 2015 Tantowi Mustofa. ttw@tantowi.com%n%n"

[Setup]
AppID=MEMCACHED64
AppName={#MyAppName}
AppVerName={#MyAppName} {#MyAppVer}
AppPublisher=memcached.org
AppPublisherURL=http://www.memcached.org/

; Support dialog on add/remove program
AppContact=Tantowi Mustofa
AppComments=Memcached64 Setup prepared by Tantowi Mustofa, ttw@tantowi.com
AppSupportPhone=+62-817-333155
AppSupportURL=http://www.tantowi.com/memcached64
AppUpdatesURL=http://www.tantowi.com/memcached64

; Setup Default
OutputDir=.
OutputBaseFileName=memcached64-{#MyAppVer}-setup
ArchitecturesAllowed=x64 

DefaultDirName={pf64}\{#MyAppDir}
DefaultGroupName={#MyAppDir}
UninstallDisplayIcon={app}\memcached.ico
ChangesAssociations=no

DisableStartupPrompt=yes
DisableDirPage=yes
DisableProgramGroupPage=yes
DisableReadyMemo=yes

[Files]
Source: "{#MyAppVer}\*"; DestDir: "{app}"; Flags: overwritereadonly

[Run]
Filename: "{app}\memcached.exe"; Parameters: "-d install"; Flags: runminimized
Filename: "{app}\memcached.exe"; Parameters: "-d start"; Flags: runminimized
Filename: "{sys}\netsh.exe"; Parameters: "advfirewall firewall add rule action=allow profile=any protocol=any enable=yes direction=in name=Memcached64 program={app}\memcached.exe";  Flags: runminimized

[UninstallRun]
Filename: "{app}\memcached.exe"; Parameters: "-d stop"; Flags: runminimized
Filename: "{app}\memcached.exe"; Parameters: "-d uninstall"; Flags: runminimized
Filename: "{sys}\netsh.exe"; Parameters: "advfirewall firewall delete rule profile=any name=Memcached64";  Flags: runminimized

[Messages]
SetupWindowTitle=%1 Setup Wizard
WelcomeLabel1=[name/ver]
WelcomeLabel2={#Welcome1}{#Welcome2}
ExitSetupMessage=If you quit now, the program will not be installed.%nExit anyway?


